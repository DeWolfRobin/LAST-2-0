while getopts ":sSi:h:" o; do
    case "${o}" in
  h)
       		usage
            	;;
  s)
          silent="true"
              ;;
  i)
          interface=${OPTARG}
              ;;
  S)
          server="true"
              ;;
	*)
            	usage
            	;;
    esac
done
shift $((OPTIND-1))

clear
printmain "Starting Tool"
createchildp createdirs
printsub "Starting up nessus"
/etc/init.d/nessusd start &
startnetcap

if [ "$silent" = "true" ]
  then
    NOP
    # arpdiscovery
    # checkresponder
else
  printsub "Nmap scans"
  pingsweep
  # nmapservice
  # python $xml2json -t xml2json -o $json $xml &
  # dnsdiscovery

  printsub "Starting nessus vulnerabillity scan"
  createchildp nessusroutine

  printsub "Starting nmap vulnerability scan"
  createchildp nmapvuln

  printsub "Starting additional scans"
  createchildp checkresponder
  createchildp additionalscan

  i=0
  while [ "$children" -ne 0 ]
  do
  	printf "waiting for L.A.S.T. to finish ($i s)\r"
  	sleep 1
    i=$(($i+1))
  done

printmain "Compiling master.json"
python plugins/createMasterJSON.py

printmain "Formatting output"
searchsploit --nmap output/nmap-output.xml --colour > output/searchsploit
fi

xvfb-run python plugins/report/report.py 2>/dev/null
printmain "Cleaning up"
# cleanup
stopnetcap

if [ "$server" = "true" ]
  then
    cd output/
printmain "Setting up server"
python -m SimpleHTTPServer 80
fi

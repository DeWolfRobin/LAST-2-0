removechildp() {
  children=$(($children-1))
}

dnsscan ()
{
  dnsrecon -d $1 -D /usr/share/wordlists/dnsmap.txt -t axfr -j $(pwd)/dnsinfo.json
}

# nmapvuln ()
# {
# nmap -Pn --script vuln -iL $hosts -oX $nmapvulnxml
# python $xml2json -t xml2json -o $nmapvulnjson $nmapvulnxml
# removechildp
# }

# nessusscan ()
# {
# # check if nessus is ready
# echo $bold$lgreen"Checking if nessus is up"$reset
# poluuid=$(http --verify=no GET https://localhost:8834/policies \X-ApiKeys:$apikeys | jq -r ".policies[0].template_uuid")
# ready=$(http --verify=no https://localhost:8834/scans \X-ApiKeys:$apikeys | jq -r ".folders")
# i=0
# while [ "$ready" = null ]
# do
# 	# printf "waiting for nessus ($i s)\r"
# 	sleep 1
# 	ready=$(http --verify=no https://localhost:8834/scans \X-ApiKeys:$apikeys | jq -r ".folders")
#   i=$(($i+1))
# done
#
# echo $bold$lgreen"Nessus is up"$reset
#
# # get hosts seperated by comma's
# hostsed=$(sed ':a;N;$!ba;s/\n/,/g' output/live-hosts.txt)
#
# echo $bold$lgreen"Creating nessus scan"$reset
# # create a new nessus scan using the predefined policy uuid
# scanid=$(echo "{\"uuid\": \"$poluuid\",\"settings\": {\"name\": \"AutoScan\",\"description\": \"a scan created by the automated script\",\"scanner_id\": 1,\"enabled\": \"true\",\"starttime\": \"20170820T100500\",\"launch\": \"YEARLY\",\"text_targets\": \"${hostsed}\"}}" | http --verify=no POST https://localhost:8834/scans \X-ApiKeys:$apikeys | jq -r ".scan.id")
# echo $scanid
#
# echo $bold$lgreen"Starting the scan"$reset
# # start the scan
# http -v --verify=no POST https://localhost:8834/scans/$scanid/launch \X-ApiKeys:$apikeys
#
# # check periodically if the scan is finished and get the export id
# i=0
# scandone=$(echo "{\"format\": \"html\", \"chapters\": \"vuln_hosts_summary;vuln_by_host;compliance_exec;remediations;vuln_by_plugin;compliance\"}" | http --verify=no POST https://localhost:8834/scans/$scanid/export \X-ApiKeys:$apikeys | jq -r ".file")
# while [ "$scandone" = null ]
# do
# 	# printf "waiting for scan ($i s)\r"
# 	sleep 1
# 	scandone=$(echo "{\"format\": \"html\", \"chapters\": \"vuln_hosts_summary;vuln_by_host;compliance_exec;remediations;vuln_by_plugin;compliance\"}" | http --verify=no POST https://localhost:8834/scans/$scanid/export \X-ApiKeys:$apikeys | jq -r ".file")
#   i=$(($i+1))
# done
# echo $scandone
#
# echo $bold$lgreen"Exporting scan"$reset
# # export is done, then save the export
# status=$(http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/status \X-ApiKeys:$apikeys | jq -r ".status")
# echo $status
# i=0
# while [ "$status" = loading ]
# do
# 	# printf "waiting for export ($i s)\r"
# 	sleep 1
# 	status=$(http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/status \X-ApiKeys:$apikeys | jq -r ".status")
#   i=$(($i+1))
# done
# http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/download \X-ApiKeys:$apikeys > output/nessus-output.html
#
# # check periodically if the scan is finished and get the export id
# i=0
# scandone=$(echo "{\"format\": \"nessus\", \"chapters\": \"vuln_hosts_summary;vuln_by_host;compliance_exec;remediations;vuln_by_plugin;compliance\"}" | http --verify=no POST https://localhost:8834/scans/$scanid/export \X-ApiKeys:$apikeys | jq -r ".file")
# while [ "$scandone" = null ]
# do
# 	# printf "waiting for scan ($i s)\r"
# 	sleep 1
# 	scandone=$(echo "{\"format\": \"nessus\", \"chapters\": \"vuln_hosts_summary;vuln_by_host;compliance_exec;remediations;vuln_by_plugin;compliance\"}" | http --verify=no POST https://localhost:8834/scans/$scanid/export \X-ApiKeys:$apikeys | jq -r ".file")
#   i=$(($i+1))
# done
# echo $scandone
#
# echo $bold$lgreen"Exporting scan"$reset
# # export is done, then save the export
# status=$(http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/status \X-ApiKeys:$apikeys | jq -r ".status")
# echo $status
# i=0
# while [ "$status" = loading ]
# do
# 	# printf "waiting for export ($i s)\r"
# 	sleep 1
# 	status=$(http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/status \X-ApiKeys:$apikeys | jq -r ".status")
#   i=$(($i+1))
# done
# http --verify=no https://localhost:8834/scans/$scanid/export/$scandone/download \X-ApiKeys:$apikeys > output/nessus-output.xml
# }

rpc(){
out=$(rpcclient -U '' -N $1 -c querydominfo | sed 's/\t//g' | sed '$!s/$/","/g' | sed 's/:/":"/g')
array=$(rpcclient -U '' -N $1 -c enumdomusers | sed 's/rid:\[.*$//g' | sed '$!s/$/,/g' | sed 's/user://g' | sed 's/ //g;s/\[//g;s/\]//g')
out="{\"${out}\",\"users\":\"[${array}]\"}"
echo $out > output/rpc-$1.json
}

enumz(){
enum4linux $1 > output/enum/enum-$1.txt
}

snmpenum(){
python plugins/snmpAutoenumEdited.py -s $1 > output/snmp/$1.txt
}

additionalscan(){
while read line; do
#rpc $line #this is deprecated since enum4linux does the same
enumz $line
#snmpenum $line # not yet implemented
done <output/live-hosts.txt
nbtscan -f output/live-hosts.txt -e > output/nbt.txt
removechildp
}

startnetcap() {
  nohup tshark -i $interface -w "output/capture.pcap" &
  wspid=`echo $!`
}

stopnetcap(){
  kill $wspid
}

# arpdiscovery(){
#   arp-scan -f config/nmap.conf -R -W "output/arp/arp.pcap" -x > "output/arp/arpscan.txt"
#   cat output/arp/arpscan.txt | awk '/\t/{print $1}' > output/arp/ips.txt
#   cat output/arp/arpscan.txt | awk '/\t/{print $2}' > output/arp/macs.txt
# }

# checkresponder(){
#   touch output/responderips.txt
#   while read line; do
#   responder-RunFinger -i $line >> output/responderips.txt
#   done <output/live-hosts.txt
#   removechildp
# }

getscope(){
nmcli dev show wlan0 | grep IP4.ADDRESS | cut -d ':' -f2 | tr -d '[:space:]' > config/nmap.conf
}

# usage(){
#   echo "Usage:\n\t$0\n\nOptions:\n\t-s\t\t Do a silent scan" 1>&2; exit 1;
# }

# cleanup() {
#   7z a report.zip output/*
#   mv output/report.* ./
#   rm -rf output/*
#   mv ./report.* output/
#   cp plugins/report/*.js output/ &
#   cp plugins/report/*.css output/ &
# }

# createdirs() {
#   mkdir output 2>/dev/null &
#   mkdir output/enum 2>/dev/null &
#   mkdir output/snmp 2>/dev/null &
#   mkdir output/arp 2>/dev/null &
#   removechildp
# }

# printsub() {
#   printf $bold$lgreen"$1"$reset
# }
#
# printmain() {
#   printf $red$bold"$1"$reset
# }
#
# NOP() {
#   prinf ""
# }

# pingsweep() {
#   nmap -n -sS -iL $conf -oG - | awk '/Up$/{print $2}' > $hosts
#   printsub "Pingsweep done"
# }

# nmapservice() {
#   nmap -sV -O -iL $hosts -oX $xml
#   printsub "Service detection done"
# }

dnsdiscovery() {
  printsub "Starting DNS scan"
  dnsscan $domain
}

# nessusroutine() {
#   nessusscan
#   python $xml2json -t xml2json -o $nessusjson $nessusxml
#   removechildp
# }

createchildp() {
  $1 &
  children=$(($children+1))
}

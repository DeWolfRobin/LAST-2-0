package main

import (
    "fmt"
    "io/ioutil"
    "flag"
    "os"
    "os/exec"
    "regexp"
    "strings"
    "bufio"
    "sync"
)

var waitgroup sync.WaitGroup
var help = flag.Bool("help, -h",false,"Show this help page")
const conf string = "config/nmap.conf"
const responderRunginger string = "output/responderips.txt"
const hosts string = "output/live-hosts.txt"
const arppcap string = "output/arp/arp.pcap"
const xml string = "output/nmap-output.xml"
const nmapvulnxml string = "output/nmapvuln.xml"
const nmapvuln2xml string = "output/nmapvuln2.xml"
const nmapvulnjson string = "output/nmapvuln.json"
const nmapvuln2json string = "output/nmapvuln2.json"
const xml2json string = "plugins/xml2json/xml2json.py"
const json string = "output/nmap-output.json"
const nessusxml string = "output/nessus-output.xml"
const nessusjson string = "output/nessus.json"
var hostsString string = ""
var domain string = "localhost"
var silent = flag.Bool("silent", false, "Use a silent scan to avoid detection")
var server = flag.Bool("server", false, "Turn on/off the report server")
var iface = flag.String("interface", "wlan0", "Select which interface to use")
var wspid string = ""
var children int = 0

func print(s string) {
  fmt.Println("[+]",s)
}

func printSub(t int, s string) {
  tabs := ""
  for i := 0; i < t; i++ {
    tabs = tabs+"\t"
  }
  fmt.Println(tabs,"[+]",s)
}

func debug(s string) {
  fmt.Println("[DEBUG]",s)
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func mkdirs(){
  os.MkdirAll("output/enum",os.ModePerm)
  os.MkdirAll("output/snmp",os.ModePerm)
  os.MkdirAll("output/arp",os.ModePerm)
}

func stringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func byteToString(input [][]byte)([]string){
  var output []string
  for _, element := range input {
    if !stringInSlice(string(element), output) {
      output = append(output,string(element))
    }
  }
  return output
}

func startNetCap(){
  // nohup tshark -i $interface -w "output/capture.pcap" &
  // wspid=`echo $!`
}

func nmapServiceDetection() {
  print("Starting nmap service detection")
  _,err := exec.Command("nmap","-sV","-O","-iL",hosts,"-oX",xml).Output()
  check(err)
  _,err = exec.Command("python",xml2json,"-t","xml2json","-o",json,xml).Output()
  check(err)
  waitgroup.Done()
  print("nmap service detection done")
}

func pingSweep(){
  print("Starting PingSweep")
  file, err := os.Create(hosts)
  check(err)
  defer file.Close()
  out,err := exec.Command("nmap","-n","-sS","-iL",conf,"-oG","-").Output()
  check(err)
  matched := regexp.MustCompile(`\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b`).FindAll(out, -1)
  file.WriteString(strings.Join(byteToString(matched),"\n"))
}

func getHostsString(){
  hostsfile, err := ioutil.ReadFile(hosts)
  check(err)
  hostsString = strings.Replace(string(hostsfile), "\n", ",", -1)
}

func checkResponderRunFinger(){
  print("Starting RunFinger")

  file, err := os.Create(responderRunginger)
  check(err)
  defer file.Close()

  hostfile, err := os.Open(hosts)
  check(err)
  defer hostfile.Close()

  fscanner := bufio.NewScanner(hostfile)
  for fscanner.Scan() {
    out,err := exec.Command("python2.7","responder-RunFinger","-i",fscanner.Text()).Output()
    check(err)
    file.WriteString(string(out))
  }
}

func compileMasterJson(){
  print("Compiling JSON")
  _,err := exec.Command("python", "plugins/createMasterJSON.py").Output()
  check(err)
}

func arpDiscovery(){
  print("Starting ARP Discovery")
  _,err := exec.Command("arp-scan","-f",conf,"-R","-W",arppcap,"-x").Output()
  check(err)
  // > "output/arp/arpscan.txt"
  // cat output/arp/arpscan.txt | awk '/\t/{print $1}' > output/arp/ips.txt
  // cat output/arp/arpscan.txt | awk '/\t/{print $2}' > output/arp/macs.txt
  waitgroup.Done()
  print("ARP Discovery done")
}

func nmapVuln(){
  print("Starting nmap vulnerability scan")
  _,err := exec.Command("nmap","-Pn","--script","vuln","-iL",hosts,"-oX",nmapvulnxml).Output()
  check(err)
  _,err = exec.Command("python",xml2json,"-t","xml2json","-o",nmapvulnjson,nmapvulnxml).Output()
  check(err)
  waitgroup.Done()
  print("nmap vulnerability scan done")
}

func logo() {
  fmt.Println(`
    ██╗         █████╗    ███████╗   ████████╗
    ██║        ██╔══██╗   ██╔════╝   ╚══██╔══╝
    ██║        ███████║   ███████╗      ██║
    ██║        ██╔══██║   ╚════██║      ██║
    ███████╗██╗██║  ██║██╗███████║██╗   ██║   ██╗
    ╚══════╝╚═╝╚═╝  ╚═╝╚═╝╚══════╝╚═╝   ╚═╝   ╚═╝
`)
}

func startup() {
  logo()
  mkdirs()
  startNetCap()
}

func formatOutput(){
  print("Formatting output")
  file, err := os.Create("output/searchsploit")
  check(err)
  defer file.Close()
  out,err := exec.Command("plugins/searchsploit/searchsploit","--nmap","'output/nmap-output.xml'","--colour").Output()
  check(err)
  file.WriteString(string(out))
  _,err = exec.Command("python","plugins/report/report.py").Output()
  check(err)
  os.Link("plugins/report/jquery.js", "output/jquery.js")
  os.Link("plugins/report/screen.css", "output/screen.css")
  os.Link("plugins/report/script.js", "output/script.js")
}

func cleanup() {
  //would be nice to upgrade from sys commands to golang
  out,err := exec.Command("7z","a","report.zip","output/*").Output()
  check(err)
  _ = out
  out,err = exec.Command("mv","output/report.*","./","&&", "rm", "-rf", "output/*", "&&", "mv", "./report.*", "output/", "&&", "cp", "plugins/report/*.js", "output/", "&&","cp", "plugins/report/*.js", "output/").Output()
  check(err)
  _ = out
}

func main(){
  flag.Parse()
  startup()
  waitgroup.Add(1)
  go arpDiscovery()
  if *silent {
    print("Initiating silent scan")
  } else {
    print("Initiating noisy scan")
    pingSweep()
    getHostsString()
    // checkResponderRunFinger()
    waitgroup.Add(1)
    go nmapServiceDetection()
    waitgroup.Add(1)
    go nmapVuln()
  }
  waitgroup.Wait()
  compileMasterJson()
  formatOutput()
  // cleanup()
}

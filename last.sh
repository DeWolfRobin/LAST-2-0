#!/bin/sh
# CONFIG
. includes/conf.sh

# COLORS
. includes/colors.sh

# FUNCTIONS
. includes/functions.sh

# OPTIONS
. includes/options.sh

#SCRIPT
. includes/main.sh
